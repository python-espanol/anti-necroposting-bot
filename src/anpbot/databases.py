import sqlite3


class DB:
    def __init__(self, db_name):
        self.db_name = db_name

    def run_query(self, query, parameters=()):
        with sqlite3.connect(self.db_name) as conn:
            cursor = conn.cursor()
            result = cursor.execute(query, parameters)
            conn.commit()
        return result

    def assign_lang(self, chat_id, lang):
        try:
            self.run_query("INSERT INTO chats VALUES(?, ?, NULL)", (chat_id, lang))
        except sqlite3.IntegrityError:  # ID already exists
            self.run_query("UPDATE chats SET lang = ? WHERE id = ?", (lang, chat_id))

    def assign_days_limit(self, chat_id, days_limit):
        try:
            self.run_query("INSERT INTO chats VALUES(?, NULL, ?)", (chat_id, days_limit))
        except sqlite3.IntegrityError:  # ID already exists
            self.run_query("UPDATE chats SET days_limit = ? WHERE id = ?", (days_limit, chat_id))

    def load_db(self, chat_lang, chat_days_limit):
        for row in self.run_query('SELECT * FROM chats'):
            if row[1]:
                chat_lang[row[0]] = row[1]
            if row[2]:
                chat_days_limit[row[0]] = row[2]
