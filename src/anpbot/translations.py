langset = {"ES", "FR", "EN"}

replying_old_message = {
    "ES": "¡El mensaje al que ha respondido tiene más de {} días! Si su respuesta ya no es relevante, por favor, elimínela.",
    "FR": "Le message auquel vous avez répondu a plus de {} jours! Si votre réponse n'est plus pertinente, veuillez la supprimer.",
    "EN": "The message you replied is more than {} days ago! If your reply isn't relevant now, please delete it."
}
