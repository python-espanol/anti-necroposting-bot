import telebot as tb
import anpbot.databases as databases
import anpbot.translations as translations
import yaml
from datetime import datetime, timedelta
from collections import defaultdict


class MainProgram:
    DB_NAME = "chats.db"

    def __init__(self):
        self.config = self.load_config()
        self.bot = tb.TeleBot(self.config["TOKEN"])

        self.chat_lang = defaultdict(lambda: "EN")
        self.chat_days_limit = defaultdict(lambda: 7)

        self.db = databases.DB(self.DB_NAME)
        self.db.load_db(self.chat_lang, self.chat_days_limit)
        self.bot.set_update_listener(self.listener)
        self.bot.polling()

    @staticmethod
    def load_config():
        file = open("config.yaml", 'r')
        config = yaml.load(file, Loader=yaml.FullLoader)
        file.close()
        return config

    def sender_is_admin(self, m):
        if self.bot.get_chat_member(m.chat.id, m.from_user.id).status in ('administrator', 'creator'):
            return True
        self.bot.reply_to(m, "Must be admin to use that command.")
        return False

    def handle_command(self, m):
        splited = m.text.split()
        if splited[0] == "/setlang" and self.sender_is_admin(m):
            if splited[1] in translations.langset:
                self.db.assign_lang(m.chat.id, splited[1])
                self.chat_lang[m.chat.id] = splited[1]
                self.bot.reply_to(m, "Configuration successfully set.")
            else:
                self.bot.reply_to(m, "Invalid language!")

        elif splited[0] == "/setdayslimit" and self.sender_is_admin(m):
            try:
                days_limit = int(splited[1])
            except ValueError:
                self.bot.reply_to(m, "Invalid number!")
            else:
                self.db.assign_days_limit(m.chat.id, days_limit)
                self.chat_days_limit[m.chat.id] = days_limit
                self.bot.reply_to(m, "Configuration successfully set.")

    def listener(self, messages):
        for m in messages:
            if m.text and m.text.startswith('/'):
                self.handle_command(m)
                continue
            reply = m.reply_to_message
            delta = timedelta(days=self.chat_days_limit[m.chat.id])
            if reply and datetime.utcfromtimestamp(reply.date) < datetime.utcnow() - delta:
                self.bot.reply_to(m,
                                  translations.replying_old_message[self.chat_lang[m.chat.id]]
                                  .format(self.chat_days_limit[m.chat.id]))


if __name__ == '__main__':
    program = MainProgram()
