## Anti Necroposting Bot

### How to use
First, create a virtual environment and activate it (optional):  
`$ python -m venv venv`  
`$ source ./venv/bin/activate` (linux)  
`$ source ./venv/bin/activate.fish` (linux, fish shell)

Then install the dependencies:  
`$ pip install -r requirements.txt`

Edit the config.yaml file, and put yours bot token.  

Finally run the code:  
`python src/main.py`